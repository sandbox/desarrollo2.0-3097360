RADIX UI components
-------------------

HOW TO USE THIS MODULE
----------------------

This module includes a library of patterns ready to use with radix and ui_patterns.

It also includes a series of modules that implement ready-made solutions for common components in the form of paragraphs. Activating one of this modules will generate a new paragraph correctly configured and ready to implement a pattern.

As of today, the patterns available as paragraphs are:
- Accordion
- WYSIWYG
- FAQ
- Image with text
- Quote

Paragraphs are considered to be the best choice to implement component-like elements, but in future developments other entities (blocks, nodes, etc...) could be added as well.

HOW TO CREATE A NEW PATTERN
---------------------------

Patterns are stored under templates/patterns and will be automatically discovered and ready to use by any Drupal installation that uses both ui_patterns (https://www.drupal.org/project/ui_patterns) and components (https://www.drupal.org/project/components).

The minimal implementation of a pattern is defined by:
- A pattern definition in a .yml file
- A twig template

All the info related to pattern definitions can be found on the module documentation (https://ui-patterns.readthedocs.io/en/8.x-1.x/content/patterns-definition.html).

By default, we use name suggestions for the twig template (the schema being pattern-name-of-the-pattern.html.twig) but you can use the "use: " property on the .yml definition to specify another template name.

Patterns can be extended by using variants and attaching libraries. See the ui patterns documentation for further reference.



PATTERN SETTINGS

Thanks to ui_pattern_settings module, it is possible to define configurations at a pattern level For example, a slider pattern can expose a number of slides to show, or a card pattern can expose a boolean that sets/unsets a border.

With the ui_patterns module, this can be achieved by using fields and rendering them to pattern's regions, but this approach isn't ideal as it treats pattern's content and configuracion as equals (i.e: exposing a class name as a field rendered on a pattern region just to be able to pass it to the template).

The 'settings' key can hold different settings that will allow configuration of the pattern without having to resort to this "fake" regions.

The settings definition can hold these types of values:

my-pattern:
  settings:
    text:
      type: textfield
      label: Text
      description: Add modifier here
    url:
      type: url
      label: Url
    attributes:
      type: attributes
      label: Attributes
    token:
      type: token
      label: Token chooser
    checkboxes:
      type: checkbox
      label: Different checkboxes
      options:
        option1: Option 1
        option2: Option 2
    select:
      type: select
      label: A select list
      options:
        option1: Option 1
        option2: Option 2

Some of them will apply some processing. For example, 'attributes' will effectively populate Drupal's 'attributes` array, 'url' will convert paths to its alias and 'token' will render the given token. The rest of the configurations will pass the user input directly to the render array.




HOW TO CREATE A NEW MODULE FOR AN ENTITY IMPLEMENTING A PATTERN
---------------------------------------------------------------

This module also includes several sub-modules that enable paragraphs ready to implement specific patterns in form of commonly used and reusable components.

Due to its nature, Paragraphs are the preferred Drupal entity to implement a pattern, but other entities could also be used.

To update an existing module:
- Make your modifications using Drupal's UI (i.e add or modify fields, modify view mode, etc...)
- Export configuration (drush cex)
- Execute ./scripts/copy-content-config-entity-to-module.sh ENTITY_TYPE BUNDLE TARGET_MODULE

To create a new module, you will need to create the TARGE_MODULE beforehand, with a .module and a.info.yml file. Then, the script will fill the module with the config.


USEFUL LINKS AND DEVELOPER INFO
-------------------------------

HOW TO RENDER A PATTERN ON A PREVIEW.

On nested patterns, it is common the need to set the preview of a pattern regiona as a pattern. For example, the "button group" pattern will populate its "items" region with patterns of the "button" type.

You can fill the preview of the pattern with other patters. Use the example on "card" on https://github.com/nuvoleweb/bootstrap_patterns/blob/master/bootstrap_patterns.ui_patterns.yml:

card:
  label: 'Card'
  description: 'A card component.'
  fields:
    image:
      type: 'image'
      label: 'Image'
      description: 'Card image.'
      preview:
        type: 'pattern'
        id: 'image'
        fields:
          image:
            theme: 'image'
            uri: 'http://lorempixel.com/400/200/nature/2'
          caption: '&copy; 2017 John Smith photography'

As you can see, the preview accepts any render array - including a pattern render array. You need to set its 'id' to your desired pattern's name and you can also populate the field list in order to configure the rendering of the nested pattern.