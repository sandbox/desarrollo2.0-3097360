/**
 * @file
 * Carousel js.
 */

 (function ($, Drupal, window, document) {
   'use strict';

   Drupal.behaviors.radix_ui_components_carousel = {
     attach: function (context, settings) {
       $('.carousel', context).once('ruc-carousel').each(function () {
         var $carousel = $(this);
         $carousel.slick();
       });
     }
   };
 })(jQuery, Drupal, this, this.document);
